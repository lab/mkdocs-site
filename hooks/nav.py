import jinja2
from mkdocs.utils.templates import url_filter
from mkdocs.plugins import get_plugin_logger

logger = get_plugin_logger("nav")

def is_list_page(page) -> bool:
    if not page.parent:
        return False

    return page.parent.children[0] == page

def children(page):
    if not is_list_page(page):
        return []

    page_children = []

    for child in page.parent.children[1:]:
        if child.is_page:
            page_children.append(child)

        # If the child is a section, use its index page as the child
        if child.is_section:
            page_children.append(child.children[0])

    return page_children

def get_parent_page(page):
    if is_list_page(page) and len(page.parent.children) == 1:
        parent = page.parent.parent
    else:
        parent = page.parent

    if parent is None:
        return None

    return parent.children[0]


@jinja2.pass_context
def get_page(context, slug):
    nav = context["nav"]
    for page in nav.pages:
        if page.file.src_uri == slug:
            return page
    logger.warning("Unable to find page for '%s'", slug)
    return None

@jinja2.pass_context
def get_page_url(context, slug):
    page = get_page(context, slug)

    if page is None:
        return None

    return url_filter(context, page.url)

def on_env(env, config, files):
    env.filters["is_list_page"] = is_list_page
    env.filters["children"] = children
    env.globals["get_page"] = get_page
    env.globals["get_page_url"] = get_page_url
    env.globals["get_parent_page"] = get_parent_page
