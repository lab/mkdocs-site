from mkdocs.utils.cache import download_and_cache_url
from datetime import timedelta
import os
import json
import jinja2
import re

UNSPLASH_CACHE_TIME = timedelta(days=14)

MD_ESCAPE_CHARS = r"\_*[]()~`>#+-=|{}.!"
MD_ESCAPE_RE = re.compile(f"([{re.escape(MD_ESCAPE_CHARS)}])")

def get_unsplash_image(image_id):
    client_id = os.environ["UNSPLASH_CLIENT_ID"]
    url = f"https://api.unsplash.com/photos/{image_id}?client_id={client_id}"
    return json.loads(download_and_cache_url(url, UNSPLASH_CACHE_TIME))


def escape_markdown(value):
    return MD_ESCAPE_RE.sub(r"\\\1", value)


def on_env(env: jinja2.Environment, config, files):
    env.globals["unsplash"] = get_unsplash_image

    env.filters["escape_markdown"] = escape_markdown

    return env
