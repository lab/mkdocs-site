from mkdocs.plugins import event_priority

@event_priority(100)
def on_page_markdown(markdown, page, config, files):
    # HACK: Calculate what the `env` should probably look like. By default, `on_env` is called after `on_page_markdown`.
    env = config.plugins.on_env(config.theme.get_env(), config=config, files=files)

    template = env.from_string(markdown)

    return template.render(page=page, config=config)
