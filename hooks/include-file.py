from jinja2_simple_tags import StandaloneTag
import os

def on_env(env, config, files):
    env.add_extension(IncludeFileTag)

    return env

class IncludeFileTag(StandaloneTag):
    tags = {"include_file"}

    def render(self, path):
        if path.startswith("/"):
            file_path = os.path.join(self.context["config"]["site_dir"], path.removeprefix("/"))
        else:
            file_path = os.path.join(os.path.dirname(self.context["page"].file.abs_src_path), path)

        with open(file_path) as f:
            return f.read()
