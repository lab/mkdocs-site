import fnmatch

from mkdocs.plugins import event_priority, get_plugin_logger

EXCLUDED_FILES = {
    "*.mmd"
}

logger = get_plugin_logger("exclude-static")

@event_priority(100)
def on_files(files, config):
    for file in files:
        if any(fnmatch.fnmatch(file.url, pattern) for pattern in EXCLUDED_FILES):
            logger.debug("Excluding %s", file.url)
            files.remove(file)

    return files
