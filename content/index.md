# Content

Before image

![Content](./assets/test.jpg)

After image

![Image](https://theorangeone.net/images/ml7eDl0dfJcNa0gb78bceOAZA2Y=/1/width-1200/header.jpg)

{{ 1 + 2 }}

- [Another page](./other.md)
- [Something else on the site](/sitemap.xml)
- [An external site](https://mkdocs.org)
